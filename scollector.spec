Summary	:	This package contains scollector, a daemon to send metrics to OpenTSDB.
Name:		scollector
Version:	0.0.1
Release:	1%{?dist}
License:	GPL
BuildArch:	x86_64
Group:		Development/Tools
Source:		%{name}-%{version}.tar.gz
BuildRoot:	%{_builddir}/%{name}-root
AutoReqProv:	no


%description
This package contains the tcollector tools to send metrics to OpenTSDB.

%prep
echo "This is prep"
%setup

%build
echo "This is build"

%install
echo "This is install"

mkdir -p $RPM_BUILD_ROOT
cp -RT files/ $RPM_BUILD_ROOT/

%clean
echo "This is clean"

%files
%defattr(-,root,root,-)
/etc/init.d/scollector
/etc/scollector/
/etc/scollector/scollector.toml
/usr/bin/scollector

%changelog
* Wed Jul 22 2015 Charles Newey <charles.newey@cern.ch> 0.1-1
- Initial creation of tcollector package.
