SPECFILE=$(shell find -maxdepth 1 -name \*.spec -exec basename {} \; )
REPOURL=git+ssh://git@gitlab.cern.ch:7999
REPOPREFIX=/cnewey

# Get all the package info from the spec file
PKGVERSION=$(shell awk '/Version:/ { print $$2 }' ${SPECFILE})
PKGRELEASE=$(shell awk '/Release:/ { print $$2 }' ${SPECFILE})
PKGNAME=$(shell awk '/Name:/ { print $$2 }' ${SPECFILE})
PKGID=$(PKGNAME)-$(PKGVERSION)
TARFILE=$(PKGID).tar.gz

sources:
	tar cvzf $(TARFILE) --exclude-vcs --transform 's,^,$(PKGID)/,' *
	rm -rf /tmp/$(PKGID)
	mkdir /tmp/$(PKGID)
	cp -rv * /tmp/$(PKGID)/
	pwd ; ls -l
	cd /tmp ; tar -czf $(TARFILE) $(PKGID)
	mv /tmp/$(TARFILE) .
	rm -rf /tmp/$(PKGID)

all:    sources

clean:
	rm $(TARFILE)

srpm:   all
	rpmbuild -bs --target=x86_64 --define '_sourcedir $(PWD)' ${SPECFILE}

rpm:    all
	rpmbuild -ba --target=x84_64 --define '_sourcedir $(PWD)' ${SPECFILE}

scratch:
	koji build db6 --scratch --arch=x64_64 ${REPOURL}${REPOPREFIX}/${PKGNAME}.git#$(shell git rev-parse HEAD)

build:
	koji build db6 --arch=x64_64 ${REPOURL}${REPOPREFIX}/${PKGNAME}.git#$(shell git rev-parse HEAD)

